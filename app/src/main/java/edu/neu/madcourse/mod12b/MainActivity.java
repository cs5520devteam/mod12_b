package edu.neu.madcourse.mod12b;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.net.URI;
import java.util.List;

public class MainActivity extends Activity implements
    LoaderManager.LoaderCallbacks<Cursor>{

    public static final Uri FRIENDS_URI = Uri.parse("content://edu.neu.madcourse.mod12/friends");

    private static int[] TO = {R.id.friend_list_item_rating,
            R.id.friend_list_item_firstName,
            R.id.friend_list_item_lastName,
            R.id.friend_list_item_rating };

    private static String[] FROM = { "_id",
            "firstName",
            "lastName",
            "rating"};

    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView myFriendsList = (ListView)findViewById(R.id.friends_cp_listView);

        getLoaderManager().initLoader(0, null, this);
        adapter = new SimpleCursorAdapter(this,
                R.layout.friend_list_item,
                null,
                FROM,
                TO,
                0);

        myFriendsList.setAdapter(adapter);
    }

    public void getFriends(View view){

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Projection = which columns should be returned
        String[] projection = FROM;
        CursorLoader cursorLoader = new CursorLoader(this,
                FRIENDS_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
